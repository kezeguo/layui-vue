## [1.0.6] (2024-4-24)

### Bug Fixes
* 修复 `Vue[3.4.24]` 版本中使用hasOwnProperty可能造成错误

## [1.0.5] (2024-4-22)

### Layui-vue Upgrade

## [1.0.4] (2024-4-2)

### Features
* 调整 `slots.customRender` 参数 ([#I9DFML](https://gitee.com/layui-vue/layui-vue/issues/I9DFML#note_26279350_link))

## [1.0.3] (2024-3-8)

### Bug Fixes
* 修复 `form` 样式无效

## [1.0.2] (2024-3-1)

### Bug Fixes
* ...

## [1.0.1] (2024-3-1)

### Bug Fixes
* 修复 `workspace` 导致 `layui-vue` 引用错误

## [1.0.0] (2024-2-29)

### Features
* 新增json-schema-form组件
